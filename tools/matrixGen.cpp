#include <iostream>
#include <math.h>
#include <random>
#include <stdlib.h>

int main(int argc, char const *argv[]){

	char *p;
	int row = strtol(argv[1], &p, 10);
	int col = strtol(argv[1], &p, 10);
	const unsigned long int base = 100000000000000;


	std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<unsigned long long> dis(1, base);
	
	std::cout << row << " " << col << std::endl;
	for (int i = 0; i < row; ++i){
		for (int j = 0; j < col; ++j){	
			std::cout << dis(gen) /*<< dis(gen)*/ << " ";
		}
		std::cout << std::endl;
	}
	
	return 0;
}
