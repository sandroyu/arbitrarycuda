CC=g++
CC_FLAGS=
CC_LIBS=

NVCC=nvcc
NVCC_FLAGS=-Xcompiler -rdynamic -G --expt-relaxed-constexpr -dc
NVCC_LIBS=

SRC_DIR = ./src
OBJ_DIR = ./obj
INC_DIR = ./include
HEADER = $(INC_DIR)/header.h

TARGET = MatrixMult

OBJS = $(OBJ_DIR)/main.o $(OBJ_DIR)/cuda_kernel.o $(OBJ_DIR)/device.o $(OBJ_DIR)/functions.o $(OBJ_DIR)/arbitrary.o 

# Link C++ and CUDA compiled object files to target executable:
$(TARGET) : $(OBJS)
	$(NVCC) $(OBJS) -o $@ 

# Compile main .cu file to object files:
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cu
	$(NVCC) $(NVCC_FLAGS) -c $< -o $@

# Compile C++ source files to object files:
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cpp $(HEADER)
	$(CC) $(CC_FLAGS) -c $< -o $@

# Compile CUDA source files to object files:
$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cu $(INC_DIR)/%.cuh
	$(NVCC) $(NVCC_FLAGS) -c $< -o $@ $(NVCC_LIBS)

# Clean objects in object directory.
clean:
	$(RM) obj/* *.o $(TARGET)
