#include "../include/header.h"

std::string operator+(const std::string &s1, const std::string &s2);

std::string operator-(const std::string &s1, const std::string &s2);

std::string operator*(const std::string &s1, const std::string &s2);

void MatrixMult(const std::string &M1, const std::string &M2, const std::string &M3);