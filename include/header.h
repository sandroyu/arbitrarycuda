#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <math.h>

#include <chrono>
#include <thread>

#define BASE 10000


int getSqMatSize(std::string, std::ifstream*);

void fillDigits(int*, std::string, int, int);