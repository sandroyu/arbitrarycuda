#pragma once

#include "header.h"

__device__ void any_carry(int*, int, int, int, int);

__device__ void vec_mul(int*, int*, int*, int, int, int, int, int);
