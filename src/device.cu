#include "../include/device.cuh"

__device__ void any_carry(int* C, int tid, int base, int p, int length){
	bool carry = C[p + tid] > (base - 1);
	int tmp = 0;
	__syncthreads();
	while (__any_sync(0xffffffff, carry)){
		tmp = C[p + tid] / base;
		C[p + tid] = C[p + tid] % base;
		__syncthreads();
		if (tid < length - 1)
			C[p + tid + 1] += tmp;
		__syncthreads();
		carry = C[p + tid] > (base - 1);
	}
}
