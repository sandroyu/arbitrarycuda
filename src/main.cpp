/* This is a program that reads bignums from an ASCII file and process them on the GPU
   the end objective will be to calculate matrix multiplication for those numbers. */
/* Implementation ideas: 3D blocks, use of shared memory for computation */

/* Author: Yu Sandro Da */

#include "../include/header.h"
#include "../include/arbitrary.h"

int main(int argc, char const *argv[])
{
	std::string mat1 = argv[1];
	std::string mat2 = argv[2];
	std::string output = argv[3];

	MatrixMult(mat1, mat2, output);
	
	return 0;
}
