#include "../include/header.h"

int getSqMatSize(std::string s, std::ifstream* in){
	int n[2] = {0, 0};
	for (int i = 0; i < 2; ++i){
		std::cout << "Reading matrix size" << std::endl;
		*in >> s;
		std::cout << s << std::endl;
		n[i] = std::stoi(s);
	}
	if (n[0] != n[1]){
		std::cerr << "Error: use square matrices " << std::endl;
		exit(-1);
	}
	return n[0];
}

void fillDigits(int* D, std::string num, int l, int off){
	int j = 0;
	for (j = 0; j < l - 1; j++)
		D[off + j] = std::stoi(num.substr(num.length() - 4 * (j + 1), 4));
	D[off + j] = std::stoi(num.substr(0, num.length() - 4 * j));
}