#include "../include/arbitrary.h"
#include "../include/cuda_kernel.cuh"

void MatrixMult(const std::string &M1, const std::string &M2, const std::string &M3){

	/* Preprocessing phase: * * * * * * * * * * * * * * * * * * * * * * */
	/* read each matrix from text file, store it into one single array of int,
	   using a base 10000 division; then, pass the resulting arrays to GPU.
	   Implementation: it's simply a matter of reading the text file 4 by 4,
	   and storing the least significant 4-digits first.
	   A first scan is done in order to find the longest number in both files:
	   every element in both arrays are set to this length, in order to have
	   better coalescing and uniform thread strides.
	   Reading, writing and string-to-int conversion are all done using STL */

	std::ifstream inA, inB;
	inA.open(M1);
	inB.open(M2);
	std::string dataA, dataB;
	int N = getSqMatSize(dataA, &inA);
	if (N != getSqMatSize(dataB, &inB)){
		std::cerr << "Error: use matrices of the same size" << std::endl;
		// return -1;
	}

	// pass the file content to the macro-array AllNums, containing base-10k digits.
	int* AllNumsA;
	int* AllNumsB;
	int* AllNumsR;

	//first read: find the longest number in both input files
	int maxLen = 0;
	for (int i = 0; i < N * N; ++i){
		inA >> dataA;
		inB >> dataB;

		if (dataA.length() > maxLen)
			maxLen = dataA.length();
		if (dataB.length() > maxLen)
			maxLen = dataB.length();
	}
	maxLen = ceil(maxLen/4.0);
	//find the maximum size of each result element
	int rLen = maxLen * 2 + ceil(log10(N));

	// Allocate memory: Unified Memory is used.
	cudaMallocManaged(&AllNumsA, sizeof(int) * N * N * maxLen);
	cudaMallocManaged(&AllNumsB, sizeof(int) * N * N * maxLen);
	cudaMallocManaged(&AllNumsR, sizeof(int) * N * N * rLen);

	// Second read to fill the arrays
	inA.seekg(0);
	inB.seekg(0);
	std::string skip;
	std::getline(inA, skip);
	std::getline(inB, skip);

	for (int i = 0; i < N * N; ++i){
		inA >> dataA;
		inB >> dataB;
		int digitsA = ceil(dataA.length()/4.0);
		int digitsB = ceil(dataB.length()/4.0);
		fillDigits(AllNumsA, dataA, digitsA, maxLen * i);
		fillDigits(AllNumsB, dataB, digitsB, maxLen * i);
	}

	/* Computation phase: * * * * * * * * * * * * * * * * * * * * * * * */
	/* standard GPU computation, memory allocation, kernel call, results */

	// 3D blocks are used: 
	// x is the depth coordinate (usually it's z)
	// y is the length coordinate (usually x)
	// z is the height coordinate (usually y)
	// this choice was made because the macro-arrays are written in such a way,
	// that the digits of every number are stored sequentially; this way,
	// the depth will be represented by the first coordinate

	int threadsX = min(maxLen, 16);
	int threadsYZ = 8;
	// number of threads per block is architecturally limited to 1024
	int blocks = (N + threadsYZ -1) / threadsYZ;

	dim3 THREADS(threadsX, threadsYZ, threadsYZ); // 3D blocks
	dim3 BLOCKS(blocks, blocks);				// 2D grid
	// dynamic shared memory needs host-side allocation
	size_t shared = sizeof(int) * threadsYZ * threadsYZ * maxLen * 2;

	// Kernel call
	matrixMul<<<BLOCKS, THREADS, shared>>>(AllNumsA, AllNumsB, AllNumsR, maxLen, rLen, N);
	cudaDeviceSynchronize();

	// Error checking
	cudaError_t error = cudaGetLastError();
	if(error != cudaSuccess){
    	// print the CUDA error message and exit
	   	std::cerr << "CUDA error: " << cudaGetErrorString(error) << std::endl;
	exit(-1);
	}


	/* Cleaning phase * * * * * * * * * * * * * * * * * * * * * * * * * */

	// close the opened files.
	inA.close();
	inB.close();

	std::cout << "printing results..." << std::endl;
	// print the result in the standard fashion

	std::ofstream ofs(M3, std::ofstream::out);
	ofs << N << " ";
	ofs << N << std::endl;
	for (int i = rLen - 1; i < N * N * rLen; i += rLen){
		//bool first_digit = true;
		for (int k = 0; k < rLen; ++k){
			// for (k = k; AllNumsR[i - k] == 0 && k < rLen; ++k)
			// 	first_digit = true;
			// if (first_digit == true){
			// 	ofs << AllNumsR[i - k] << " ";
			// 	k++;
			// 	first_digit = false;
			// }
			ofs << std::setfill('0') << std::setw(4) <<  AllNumsR[i - k];
		}
		ofs << std::endl;
	}

	ofs.close();
	
	// Freeing memory
	cudaFree(AllNumsA);
	cudaFree(AllNumsB);
	cudaFree(AllNumsR);

	std::cout << " program end " << std::endl;
}