#include "../include/header.h"
#include "../include/cuda_kernel.cuh"
#include "../include/device.cuh"

__global__ void matrixMul(int* A, int* B, int* R, int l, int R_l, int n){

	extern __shared__ int SharedArray[];

	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int tz = threadIdx.z;
	int dim = blockDim.y;
	int row = blockIdx.y * blockDim.z + tz;
	int col = blockIdx.x * blockDim.y + ty;
	int pos = row * n + col;
	if (tx < R_l)
		R[pos * R_l + tx] = 0;
	__syncthreads();

	int l_steps = ceilf((l)/(float)blockDim.x);
	int r_steps = ceilf((R_l)/(float)blockDim.x);

	int partial_sum = 0;

	int *Sh_A = SharedArray;
	int *Sh_B = &Sh_A[l * dim * dim];
	if ( pos < n * n ){
		for (int i = 0; i < ((n + dim - 1) / dim); ++i){
			//fill Shared Memory submatrices
			for (int j = 0; j < l_steps; ++j){
				int t_j = tx + j * blockDim.x;
				if (t_j < l){
					Sh_A[t_j + tz * dim * l + ty * l] = A[row * n * l + i * dim * l + ty * l + t_j];
					Sh_B[t_j + tz * dim * l + ty * l] = B[i * dim * n * l + tz * n * l + col * l + t_j];
				}
			}
			__syncthreads();
			for (int k = 0; k < dim; ++k){
				for (int j = 0; j < l_steps; ++j){
					int t_j = tx;
					if (t_j < l){
						for (int q = 0; q < l; ++q){
							partial_sum = Sh_A[tz * dim * l + k * l + q] * Sh_B[k * dim * l + ty * l + t_j];
							R[pos * R_l + t_j + q] += partial_sum; //this writes the naive multiplication in every element of the result vector buffer, for every *position* of the result matrix.
							//partial "line result" will be managed in the step below
						}
					}
				}
				__syncthreads();
				for (int j = 0; j < r_steps; ++j){
					int t_j = tx + j * blockDim.x;
					if (t_j < R_l){
						any_carry(R, t_j, BASE, pos * R_l, R_l);
						__syncthreads();
					}
				}
				__syncthreads();
			}
		}
	}
}
